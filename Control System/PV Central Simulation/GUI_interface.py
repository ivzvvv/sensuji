#! /usr/bin/python3
import tkinter as tk
from tkinter.font import Font
import Serial2DB_API
import asyncio
import serial
import threading
import subprocess
from tkinter import filedialog
import serial.tools.list_ports
from tkinter import ttk

CLEANING_CYCLE_TIME_ONE = 10
CLEANING_CYCLE_TIME_TWO = 20
CLEANING_CYCLE_TIME_THREE = 60

MEASUREMENT_CYCLE_TIME_ONE = 10
MEASUREMENT_CYCLE_TIME_TWO = 20
MEASUREMENT_CYCLE_TIME_THREE = 60

CLEANING_TIME_FAST = 3
CLEANING_TIME_LONG = 10
def check_success():
    text = text_area.get("1.0", tk.END).strip()  # Get the text from the widget
    if "SUCCESS" in text:
        start = "1.0"
        while True:
            index = text_area.search("SUCCESS", start, stopindex=tk.END)
            if not index:
                break
            end = f"{index}+{len('SUCCESS')}c"  # Calculate the end index of the found text
            text_area.tag_add("green", index, end)  # Apply the "green" tag to the found text
            start = end
def check_error():
    text = text_area.get("1.0", tk.END).strip()  # Get the text from the widget
    if "ERROR" in text:
        start = "1.0"
        while True:
            index = text_area.search("SUCCESS", start, stopindex=tk.END)
            if not index:
                break
            end = f"{index}+{len('SUCCESS')}c"  # Calculate the end index of the found text
            text_area.tag_add("red", index, end)  # Apply the "green" tag to the found text
            start = end

def display_output(output, send_to_pico=False):
    text_area.config(state=tk.NORMAL)  # Enable editing temporarily
    text_area.insert(tk.END, output + "\n")
    text_area.config(state=tk.DISABLED)
    text_area.see(tk.END)
    check_success()
    check_error()
    if send_to_pico:
        if "MC" in output:
            serial_port.write(("MC|"+output.split(" ")[3]).encode('utf-8'))
        elif "CC" in output:
            serial_port.write(("CC|"+output.split(" ")[3]).encode('utf-8'))
        elif "CT" in output:
            serial_port.write(("CT|"+output.split(" ")[3]).encode('utf-8'))
        elif "Cleaning" in output:
            serial_port.write(("CN").encode('utf-8'))
        elif "Measurements" in output:
            serial_port.write(("MN").encode('utf-8'))


def choose_serial_port():
    ports = serial.tools.list_ports.comports()
    port_names = [port.device for port in ports]
    
    serial_port = ttk.Combobox(configurer, values=port_names)
    serial_port.place(x=10,y=50)
    #serial_port.pack()

    def get_selected_port():
        global selected_port 
        selected_port = serial_port.get()
        configurer.destroy()
    
    select_button = tk.Button(configurer, text="Select", command=get_selected_port)
    select_button.place(x=150,y=150)

stop_reading = False

async def serial_listener(serial_port):
    display_output("Listening to serial port...")
    serial_port.reset_input_buffer()
    while not stop_reading:
        message = await loop.run_in_executor(None, serial_port.readline)
        print(message.decode('utf-8'))
        # Do something with the message here
        display_output(Serial2DB_API.parse_codes(message, conf_path))


def start_serial_loop():
    loop.run_forever()

def change_conf_path():
    file_path = filedialog.askopenfilename()
    global conf_path
    conf_path = file_path
    conf_disp.config(text="Selected Path: " + file_path)
    #display_output("Changed conf.ini file path to: " + conf_path)

configurer = tk.Tk()
configurer.title("Sensuji API")
configurer.geometry("600x200")
configurer.resizable(False, False)

title_label = tk.Label(configurer, text="Please select USB port and database config file")
title_label.place(x=150, y=10)

conf_path_change_button = tk.Button(configurer, text="Change conf.ini Path", command=change_conf_path)
conf_path_change_button.place(x=10, y=80)

conf_disp = tk.Label(configurer, text="", font=("Helvetica", 12))
conf_disp.place(x=10, y=120)

choose_serial_port()
configurer.mainloop()

# Create the main window
window = tk.Tk()
window.title("Sensuji API")

# Set the fixed window size
window.geometry("800x400")
window.resizable(False, False)

text_label_mc = tk.Label(window, text="Change Measurement Cycle")
text_label_mc.place(x=10, y=50)

# Create buttons for MC
button10s_mc = tk.Button(window, text=str(MEASUREMENT_CYCLE_TIME_ONE)+" secs", command=lambda: display_output("Changed MC to "+str(MEASUREMENT_CYCLE_TIME_ONE)+" seconds!", True))
button10s_mc.place(x=50, y=80)

button20s_mc = tk.Button(window, text=str(MEASUREMENT_CYCLE_TIME_TWO)+" secs", command=lambda: display_output("Changed MC to "+str(MEASUREMENT_CYCLE_TIME_TWO)+" seconds!", True))
button20s_mc.place(x=50, y=110)

button60s_mc = tk.Button(window, text=str(MEASUREMENT_CYCLE_TIME_THREE)+" secs", command=lambda: display_output("Changed MC to "+str(MEASUREMENT_CYCLE_TIME_THREE)+" seconds!", True))
button60s_mc.place(x=50, y=140)

text_label_cc = tk.Label(window, text="Change Cleaning Cycle")
text_label_cc.place(x=300, y=50)

# Create buttons for CC
button10s_cc = tk.Button(window, text=str(CLEANING_CYCLE_TIME_ONE)+" secs", command=lambda: display_output("Changed CC to "+str(CLEANING_CYCLE_TIME_ONE)+" seconds!", True))
button10s_cc.place(x=340, y=80)

button20s_cc = tk.Button(window, text=str(CLEANING_CYCLE_TIME_TWO)+" secs", command=lambda: display_output("Changed CC to "+str(CLEANING_CYCLE_TIME_TWO)+" seconds!", True))
button20s_cc.place(x=340, y=110)

button60s_cc = tk.Button(window, text=str(CLEANING_CYCLE_TIME_THREE)+" secs", command=lambda: display_output("Changed CC "+str(CLEANING_CYCLE_TIME_THREE)+" seconds!", True))
button60s_cc.place(x=340, y=140)

text_label_ct = tk.Label(window, text="Cleaning Type")
text_label_ct.place(x=600, y=50)

button3s_ct = tk.Button(window, text="Fast", command=lambda: display_output("Changed CT to "+str(CLEANING_TIME_FAST)+" seconds!", True))
button3s_ct.place(x=640, y=80)

button10s_ct = tk.Button(window, text="Long", command=lambda: display_output("Changed CT to "+str(CLEANING_TIME_LONG)+" seconds!", True))
button10s_ct.place(x=640, y=110)


clean_now = tk.Button(window, text="Clean Now!", command=lambda: display_output("Cleaning in progress!", True))
clean_now.configure(bg="green")
clean_now.place(x=480, y=160)

measure_now = tk.Button(window, text="Measure Now!", command=lambda: display_output("Measurements taken!", True))
measure_now.configure(bg="green")
measure_now.place(x=640, y=160)

# Create a text display area
title_label = tk.Label(window, text="Welcome to Sensuji API!")
title_label.place(x=270, y=10)
bold_font = Font(title_label, title_label.cget("font"))
bold_font.configure(weight="bold", size=16)
title_label.config(font=bold_font)

# Create a scrollbar
scrollbar = tk.Scrollbar(window)
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

text_area = tk.Text(window, height=10, width = 95)
text_area.place(x=10, y=200)
scrollbar.config(command=text_area.yview)
text_area.config(yscrollcommand=scrollbar.set)
text_area.configure(state=tk.DISABLED)  # Make it non-editable
text_area.configure(bg="black")  # Set background color
text_area.configure(fg="white")  # Set text color

text_area.tag_config("red", foreground="red")
text_area.tag_config("green", foreground="green")

serial_port = serial.Serial(selected_port, 9600)  # Replace 'COM1' with your serial port and '9600' with the baud rate
loop = asyncio.get_event_loop()
task = loop.create_task(serial_listener(serial_port))

# Start the main loop
#window.protocol("WM_DELETE_WINDOW", on_window_close)
thread = threading.Thread(target=start_serial_loop)
# Start the thread
thread.start()

window.mainloop()
stop_reading = True
task.cancel()
loop.stop()
