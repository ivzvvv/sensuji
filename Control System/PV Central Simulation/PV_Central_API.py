#!/usr/bin/env python3
import sys
import atexit
import os 
import serial
import argparse

MAIN = 1
SEND_COMMAND = 2
RECEIVE_VALUE = 3
VALUES_RECEIVED = 1
COMMAND_SENT = 2
COMMAND_NOT_SENT = -1
WRONG_CODE = -2
WRONG_FORMAT = -3

ARGPARSE_OK = 0
valid_choices_main = ["1", "2", "3"]
valid_codes_measurements = ["AA", "SD", "SP", "CD", "CC", "RS", "TS", "HS"]
valid_codes_control = ["MC", "ST", "EX"]

control_codes_format = {
    "MC" : 1,
    "ST" : 1,
    "EX" : 2
}

color_code_title   = '\033[36m'   # Cyan
color_code_success = '\033[32m'   # Green
color_code_error   = '\033[31m'   # Red
color_code_end     = '\033[0m'

def exit_handler():
    if ARGPARSE_OK == 1:
        clear_terminal()

def clear_terminal():
    print("\033[H\033[J")

def choices_sendCommand():
    print("                                  ------------------------------------                                       ")
    print("                                  | "+color_code_title+"Send control commands to station "+color_code_end+"|                                       ")
    print("                                  ------------------------------------                                       ")
    print("Please enter the CODE and the VALUES of the command you want to send in order and in the followign format:   \n")
    print("                                 CODE,value1,value2, ..., valueN                                             \n")
    print("Here is a table with available codes:                                                                        ")
    print("------------------------------------------------------------------------------------------------------------------")
    print("| MC | MeasurementCycle | Changes the time it takes for the station to send data (in seconds)                ")
    print("| ST | SoilingThreshold | Changes the the threshold for determining whether there is soilign or not (0-100)  ")
    print("| EX | Value1,Value2    | Example entry with 2 values     ")
    print("------------------------------------------------------------------------------------------------------------------")
    print("[1] Go back                                                                                                  ")
    print("[2] Exit                                                                                                     ")

    if COMMAND_SENT in options:
        print(color_code_success + "\n["+options[options.index(COMMAND_SENT)+1]+"] Command sent to Soiling Station!" + color_code_end)
    
    if COMMAND_NOT_SENT in options:
        print(color_code_error + "\n["+options[options.index(COMMAND_NOT_SENT)+1]+"] There was an error writing to the serial port. Check if it is connected correctly." + color_code_end)

    if WRONG_CODE in options:
        print(color_code_error + "\n["+options[options.index(WRONG_CODE)+1]+"] Command not recognized." + color_code_end)

    if WRONG_FORMAT in options:
        print(color_code_error + "\n["+options[options.index(WRONG_FORMAT)+1]+"] Wrong format. Check table for number of values and their order." + color_code_end)
def choices_receiveValue():
    print("                                  --------------------------------                                           ")
    print("                                  | "+color_code_title+"Receive values from station "+color_code_end+" |                                           ")
    print("                                  --------------------------------                                           ")
    print("Please enter the CODE of of the values you would like to receive.")
    print("Here is a table with available codes: ")
    print("-------------------------------------------------------------------------------------------------------------")
    print("| AA | SoilingPercentage, CurrentCleanPanel, CurrentDirtyPanel, RainSensor, TemperatureSensor, HumiditySensor")
    print("| SD | CurrentCleanPanel, CurrentDirtyPanel, RainSensor, TemperatureSensor, HumiditySensor                   ")
    print("| SP | SoilingPercentage                                                                                     ")
    print("| CD | CurrentDirtyPanel                                                                                     ")
    print("| CC | CurrentCleanPanel                                                                                     ")
    print("| RS | RainSensor                                                                                            ")
    print("| TS | TemperatureSensor                                                                                     ")
    print("| HS | HumiditySensor                                                                                        ")
    print("-------------------------------------------------------------------------------------------------------------")
    print("[1] Go back                                                                                                  ")
    print("[2] Exit                                                                                                     ")

    if VALUES_RECEIVED in options:
        print(color_code_success + "\n["+options[options.index(VALUES_RECEIVED)+1]+"] Command sent to Soiling Station. Check Database/Grafana for updates!" + color_code_end)
    
    if COMMAND_NOT_SENT in options:
        print(color_code_error + "\n["+options[options.index(COMMAND_NOT_SENT)+1]+"] There was an error writing to the serial port. Check if it is connected correctly." + color_code_end)

    if WRONG_CODE in options:
        print(color_code_error + "\n["+options[options.index(WRONG_CODE)+1]+"] Command not recognized." + color_code_end)

def choices_main():
    print("                                  --------------------------------                                           ")
    print("                                  |    "+color_code_title+"Welcome to SenSuji API  "+color_code_end+"  |                                           ")
    print("                                  --------------------------------                                           ")
    print("What would you like to do?                                                                                   ")
    print("[1] Send Commands                                                                                            ")
    print("[2] Receive a value                                                                                          ")
    print("[3] Exit                                                                                                     ")

def send_command(code):
    code_list = list(code)
    code = [c for c in code_list if c not in ['\n', ' ']]
    code = "".join(code).replace(',', '|')

    bytes_written = serial_port.write(code.encode('utf-8'))

    if bytes_written == len(code):
        # Write successful 
        return 1, code
    else:
        # Write failed
        print('Write failed')
        return 0, ''

def check_command_formatting(command):
    command = command.replace('\n', '').replace(' ', '').split(',')
    command = [c for c in command if c != '']
    if control_codes_format[command[0]] != len(command)-1:
        return -1

    for i in range(1, control_codes_format[command[0]]+1):
        if not command[i].isnumeric():
            return -1

    return 1

def print_menu():
    if current_menu == MAIN:
        choices_main()
    elif current_menu == SEND_COMMAND:
        choices_sendCommand()
    elif current_menu == RECEIVE_VALUE:
        choices_receiveValue()


atexit.register(exit_handler)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Send commands to the Soiling Station.')
    parser.add_argument('port', help='the serial port to use (e.g. /dev/ttyUSB0)')
    parser.add_argument('baudrate', help='the baudrate to use (e.g. 9600)')
    args = parser.parse_args()

    
    serial_port = serial.Serial(args.port, args.baudrate)

    ARGPARSE_OK = 1

    clear_terminal()
    current_menu = MAIN
    next_menu = MAIN
    options = []
    while 1:
        clear_terminal()
        current_menu = next_menu
        print_menu()
        options = []
        
        get_input = input().upper()
        if current_menu == MAIN:
            if get_input not in valid_choices_main:
                continue
            elif get_input == "3":
                sys.exit(1)
            elif get_input == "1":
                next_menu = SEND_COMMAND
            elif get_input == "2":
                next_menu = RECEIVE_VALUE
        elif current_menu == SEND_COMMAND:
            if get_input == "2":
                sys.exit(1)
            elif get_input == "1":
                next_menu = MAIN
                continue

            if get_input.split(',')[0] not in valid_codes_control:
                options = [WRONG_CODE, get_input]
                continue

            if check_command_formatting(get_input) == -1:
                options = [WRONG_FORMAT, get_input]
                continue

            res, code_sent = send_command(get_input)
            if res == 1:
                options = [COMMAND_SENT, code_sent]
            elif res == 0:
                options = [COMMAND_NOT_SENT, get_input]

        elif current_menu == RECEIVE_VALUE:
            if get_input == "2":
                sys.exit(1)
            elif get_input == "1":
                next_menu = MAIN
                continue
            if get_input not in valid_codes_measurements:
                options = [WRONG_CODE, get_input]
                continue

            res, code_sent = send_command(get_input)
            if res == 1:
                options = [VALUES_RECEIVED, code_sent]
            elif res == 0:
                options = [COMMAND_NOT_SENT, get_input]