# PV Central Simulation

The Graphical API has only been developed for linux and tested on Ubuntu.

To install dependencies run:
```
    pip install -r dependencies.txt
```

If you want to run the GUI as a binary, run:
```
    pip install pyinstaller
    pyinstaller GUI_interface.py
```

 - [ ] PV_Central_API.py is a terminal, unfinished, API.
 - [ ] Serial2DB_API.py is the API to interact with the serial port and the database. Doesn't have GUI and needs to be running in order to receive data from the uC and send it to the database.
 - [ ] GUI_interface.py is the graphical interface which utilizes Serial2DB_API.py to interact with serial port and database.