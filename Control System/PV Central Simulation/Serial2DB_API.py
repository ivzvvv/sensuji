#! /usr/bin/python3
import serial
import asyncio
import argparse
import mysql.connector
import configparser

valid_codes = ["AA", "SD", "SP", "CD", "CC", "RS", "TS", "HS"]
all_measurements = {
    "SoilingPercentage" : 0, 
    "CurrentCleanPanel" : 1, 
    "CurrentDirtyPanel" : 2, 
    "RainSensor" : 3, 
    "TemperatureSensor" : 4, 
    "HumiditySensor" : 5
}
#| AA | SoilingPercentage, CurrentCleanPanel, CurrentDirtyPanel, RainSensor, TemperatureSensor, HumiditySensor
#| SD | CurrentCleanPanel, CurrentDirtyPanel, RainSensor, TemperatureSensor, HumiditySensor                   
#| SP | SoilingPercentage                                                                                     
#| CD | CurrentDirtyPanel                                                                                     
#| CC | CurrentCleanPanel                                                                                     
#| RS | RainSensor                                                                                            
#| TS | TemperatureSensor                                                                                     
#| HS | HumiditySensor       
# 
# TODO: This is done so to make the function to insert values into the DB more general
# 
# Somethng like this:
# m = codes_dict[code]
# for meas in m:
#   key = [k for k, v in all_measurements.items() if v == meas][0] -> get key aka "SoilingPercentage", "RainSensor" strings - keys in all_measurements
#   then use this key to insert into the database 
#                                                                                  
codes_dict = {
    "AA": [all_measurements["SoilingPercentage"], all_measurements["CurrentCleanPanel"], all_measurements["CurrentDirtyPanel"], all_measurements["RainSensor"], all_measurements["TemperatureSensor"], all_measurements["HumiditySensor"]], 
    "SD": [all_measurements["CurrentCleanPanel"], all_measurements["CurrentDirtyPanel"], all_measurements["RainSensor"], all_measurements["TemperatureSensor"], all_measurements["HumiditySensor"]], 
    "SP": [all_measurements["SoilingPercentage"]],
    "CD": [all_measurements["CurrentDirtyPanel"]], 
    "CC": [all_measurements["CurrentCleanPanel"]], 
    "RS": [all_measurements["RainSensor"]], 
    "TS": [all_measurements["TemperatureSensor"]], 
    "HS": [all_measurements["HumiditySensor"]]
}

def parse_codes(received,conf_path):
    received = received.split(b'|')
    #   Decode from bytes-like to string
    received = [r.decode('utf-8') for r in received]
    received[-1] = received[-1].replace('\n', '')
    received_code = received[0]

    #   TODO: get time and print the time at which the error/success has been reported
    #         based on DB time

    if received_code not in valid_codes:
        print('\033[31m' + '[ERROR]' + '\033[0m' + ' Received code not in dictionary')
        return

    if len(codes_dict[received_code]) != len(received)-1:
        print('\033[31m' + '[ERROR]' + '\033[0m' + ' Wrong number of values for received code')
        return

    c = dict()
    for i in range(0, len(codes_dict[received_code])):
        key = [k for k, v in all_measurements.items() if v == codes_dict[received_code][i]][0]
        c[key] = received[i+1]
    
    #   For each key in c, insert into DB
    #                              e.g.: key1 SoilingPercentage, key2 CurrentCleanPanel ...
    #   SQL query : 'INSERT into MEASUREMENTS (key1, key2, key3) values (c[key1], c[key2], c[key3]);'
    res, rid, created_at = insert_measurements_into_db(c, conf_path)

    if res == 0:
        print('[' + str(created_at) + ']' + '\033[31m' + '[ERROR]' + '\033[0m' + ' Inserting values into DB')
    else:
        print('[' + str(created_at) + ']' + '\033[32m' + '[SUCCESS]' + '\033[0m' + ' Inserted into DB values for the Code - '+received_code+' - and RowId - '+ str(rid) + ' - ')

def insert_measurements_into_db(c, conf_path): 
    cur, conn = connect_2_db(conf_path)

    if cur == None and conn == None:
        return 0, 0, 0
    

    keys = c.keys()
    columns = ""
    values = ""
    for key in keys:
        columns += "," + key
        values  += "," + c[key]
    columns = columns[1:]
    values  = values[1:]

    Q = "INSERT into measurements ("+columns+") values ("+values+");"

    cur.execute(Q)
    conn.commit()

    last_insert_id = cur.lastrowid

    select_query = "SELECT created_at FROM measurements WHERE id = %s"
    cur.execute(select_query, (last_insert_id,))
    created_at = cur.fetchone()[0]

    close_db_connection(cur,conn)
    return 1, last_insert_id, created_at

#   Change this function depending on the database used
def connect_2_db(conf_path):
    try:
        # Set up the connection parameters
        #   Credentials for the project
        #user="stamgr",
        #password="SoilingStation2223",
        #host="127.0.0.1",
        #database="soilingstation"
        config = configparser.ConfigParser()
        config.read(conf_path)
        conn = mysql.connector.connect(
            user=config['database']['user'],
            password=config['database']['password'],
            host=config['database']['host'],
            database=config['database']['database']
        )

        # Create a cursor object to execute SQL queries
        cur = conn.cursor()

        return cur, conn

    except mysql.connector.Error as e:
        # Handle the exception
        print(f"Error connecting to MySQL database: {e}")
        return None, None

def current_time(cur):
    cur.execute("SELECT NOW()")
    result = cur.fetchone()
    return result[0]

def close_db_connection(cur, conn):
    # Close the cursor and connection
    cur.close()
    conn.close()

async def handle_serial_input(serial_port):
    while True:
        message = await loop.run_in_executor(None, serial_port.readline)
        # Do something with the message here
        parse_codes(message)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read data asynchronously from a serial port.')
    parser.add_argument('port', help='the serial port to use (e.g. /dev/ttyUSB0)')
    parser.add_argument('baudrate', help='the baudrate to use (e.g. 9600)')
    args = parser.parse_args()
    
    loop = asyncio.get_event_loop()
    serial_port = serial.Serial(args.port, args.baudrate)

    # Start listening for incoming messages
    loop.create_task(handle_serial_input(serial_port))

    # Run the event loop
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    # Clean up the serial port and close the event loop
    serial_port.close()
    loop.close()

