#include "Arduino.h"
#include <string>
#include <Wire.h>
#include <DHT.h> //library file downloaded
#include <Adafruit_INA219.h> // library file downloaded

#ifndef SENSOR_H
#define SENSOR_H

class Sensor {
public:
    Sensor(std::string sensorType);  // constructor
    ~Sensor(); // destructor
    
    void activate(); // activate the sensor
    void deactivate(); // deactivate the sensor
    
    float readValue(int ht); 
    
    float readHumidity();
    float readTemperature();
    float readRain();
    float readCurrent(std::string& panel);
    float readCurrentFromPins(Adafruit_INA219 *ina219);

private:
    bool activated;
    std::string typeOfSensor;
    std::string humidity = "Humidity";
    std::string temperature = "Temperature";
    std::string rain = "Rain";
    std::string cleanPanelCurrent = "CleanPanelCurrent";
    std::string dirtyPanelCurrent = "DirtyPanelCurrent";
    std::string HT = "HumidityTemperature";
};

#endif // SENSOR_H
