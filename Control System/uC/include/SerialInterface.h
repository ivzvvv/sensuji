#include <string>
#include <vector>
#include "hardware/gpio.h"
#include "hardware/uart.h"

using namespace std;
// sujo é clean
//  | AA | SoilingPercentage, CurrentCleanPanel, CurrentDirtyPanel, RainSensor, TemperatureSensor, HumiditySensor
//  | SD | CurrentCleanPanel, CurrentDirtyPanel, RainSensor, TemperatureSensor, HumiditySensor                   
//  | SP | SoilingPercentage                                                                                     
//  | CD | CurrentDirtyPanel                                                                                     
//  | CC | CurrentCleanPanel                                                                                     
//  | RS | RainSensor                                                                                            
//  | TS | TemperatureSensor                                                                                     
//  | HS | HumiditySensor  

class SerialInterface{
    public:
        vector<string> ControlCodes = {"MC", "CC", "CT", "CN", "MN"};
        vector<int> CC_numberOfValues = {1,1,1,0,0};
        int numberOfControlCodes = 5;

        SerialInterface();
        ~SerialInterface();
        void writeData(float* arr, int size);
        int readCode();
        int codeExists(int c1,int c2);
        float readValue();
        string getCode(int pos);
    private:
};