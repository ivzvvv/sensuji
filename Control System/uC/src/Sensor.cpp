#include "Arduino.h"
#include "Sensor.h"
#include <Wire.h>
#include <Adafruit_INA219.h>
// library download https://github.com/RobTillaart/Arduino/tree/master/libraries/INA219

#define DHTPIN 22     // Digital pin connected to DHT11 - pin 29 on pico
//#define sensorPower 7 //rain sensor
#define sensorPin 26   // rain sensor pin number 31 on pico
#define DHTTYPE DHT11   // DHT 11
#define CLEAN_PANEL_PIN 2
#define DIRTY_PANEL_PIN 20

//  SCL - pin 7 on pico
//  SDA - pin 6 on pico

DHT dht(DHTPIN, DHTTYPE);
Adafruit_INA219 ina219_clean(0x40);   
Adafruit_INA219 ina219_dirty(0x41);   

Sensor::Sensor(const std::string sensorType) {
    typeOfSensor = sensorType;
}

Sensor::~Sensor() {
}

void Sensor::activate() {
    //put here the ina219.begin(), and call this function in the setup()??
    if     (typeOfSensor.compare(cleanPanelCurrent)){
        ina219_clean.begin();
    }
    else if(typeOfSensor.compare(dirtyPanelCurrent) ){
        ina219_dirty.begin();
    }
    else if(typeOfSensor.compare(HT)){
        dht.begin();
    }
    
}

void Sensor::deactivate() {

}

float Sensor::readValue(int ht = 0){
    float valueRead = 0;
    
    if (typeOfSensor.compare(HT) == 0){
        if      (ht == 0){
            valueRead = readHumidity();
        }
        else if (ht == 1){
            valueRead = readTemperature();
        }
    }
    else if(typeOfSensor.compare(rain) == 0){
        valueRead = readRain();
    }
    else if(typeOfSensor.compare(cleanPanelCurrent) == 0){
        valueRead = readCurrent(cleanPanelCurrent);
    }
    else if(typeOfSensor.compare(dirtyPanelCurrent) == 0){
        valueRead = readCurrent(dirtyPanelCurrent);
    }

    return valueRead;
}

float Sensor::readHumidity(){
    // According to: https://mikaelpatel.github.io/Arduino-DHT/da/da7/classDHT.html

    //dht.begin(); // begin isnt a method of the class dht
    delay(10);
    float h = dht.readHumidity();
    //dht.sleep(); // sleep isnt a method of class dht
    return h;
}

float Sensor::readTemperature(){
    //dht.begin();
    delay(10);
    float t = dht.readTemperature();
    //dht.sleep(); there is no method called sleep in class dht
    return t;
}


float Sensor::readRain(){
    //int val ; ????
 
    //delay(1000);
    //digitalWrite(sensorPower, HIGH);
    //delay(10);
    float val = analogRead(sensorPin);
    //digitalWrite(sensorPower, LOW);
    
    return val;
}

float Sensor::readCurrent(std::string& panel){
    float valueRead = 0;
    if(panel.compare(cleanPanelCurrent) == 0){
        valueRead = readCurrentFromPins(&ina219_clean);
    }
    else if(panel.compare(dirtyPanelCurrent) == 0){
        valueRead = readCurrentFromPins(&ina219_dirty);
    }
    return valueRead;
}

float Sensor::readCurrentFromPins(Adafruit_INA219 *ina219){
    //float valueRead = readCurrentPins(pin);
    
    float shuntvoltage = 0;
    float busvoltage = 0;
    float current_mA = 0;
    float loadvoltage = 0; 
    float power_mW = 0;    

    //shuntvoltage = ina219.getShuntVoltage_mV();
    //busvoltage = ina219.getBusVoltage_V();
    current_mA = (*ina219).getCurrent_mA();
    //power_mW = ina219.getPower_mW();
    //loadvoltage = busvoltage + (shuntvoltage / 1000);

    return current_mA;
}
