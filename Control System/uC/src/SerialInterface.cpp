#include "SerialInterface.h"
#include <Arduino.h>

#define UART_ID uart0
#define UART_TX_PIN 0
#define UART_RX_PIN 1
#define BAUDRATE 9600
#define DATA_BITS 8
#define STOP_BITS 1
#define MAX_BUFFER_SIZE 256

#define CONTROL_CODES__RETURN_OFFSET 200

using namespace std;

SerialInterface::SerialInterface()
{

}

SerialInterface::~SerialInterface(){

}

void SerialInterface::writeData(float* arr, int size)
{
    String result = "AA|";

    for(int i=0; i<size; i++) {
        result.concat(String(arr[i], 3));
        result.concat("|");
    }
    result.concat("\n");
    Serial1.write(result.c_str());
}

int SerialInterface::codeExists(int c1, int c2)
{
    int pos = -1;

    for(int code = 0; code < numberOfControlCodes; code ++){
        if (c1 == ControlCodes[code][0] &&
            c2 == ControlCodes[code][1])
            {
                pos = code;
            }
    }

    return pos;
}

string SerialInterface::getCode(int pos){
    return ControlCodes[pos];
}

/**
 * Reads float values from the serial interface and stores them in an array. The values are sent byte by byte, 
 * separated by a '|' character. The function reads each character from the serial interface and appends it to a 
 * number. If a decimal point is encountered, the function switches to decimal mode and appends the decimal part to 
 * the number. Once the '|' character is encountered, the current number is stored in the array and the function 
 * moves on to the next value. 
 * 
 * @param nValues The number of float values to read from the serial interface.
 * @param values A reference to an array of float values where the read values will be stored.
 * @return A pointer to the array of float values.
 */

float SerialInterface::readValue(){
    //int readValues = 0;
    float number = 0;
    bool is_decimal = false;
    float decimal_place = 10;

    int c = Serial1.read(); // read the first "|"
    while(Serial1.available() > 0){
        c = Serial1.read();
        
        if(c == '|'){
            break;
        }

        if (c == '.') { // if decimal point is encountered, switch to decimal mode
            is_decimal = true;
            continue;
        }
        if (c >= '0' && c <= '9') { // if character is a digit
            if (!is_decimal) { // if we haven't encountered a decimal point yet, append to left side of number
                number = number * 10 + (c - '0');
            } else { // if we have encountered a decimal point, append to right side of number
                number += (c - '0') / decimal_place;
                decimal_place *= 10;
            }
        }
    }

    return number;
}


/**
 * @brief This function is to be used when the uC is idle and in between measurement cycles.
 *        Constantly reads from the serial port to check if any command has been received from the PV central
 *        Idea for usage: the return of this function can directly say by its index the code received, then with the same
 *        index we know how many values we need to receive (control codes) or how many values we need to send (measurement codes)
 * 
 * @return int Returns integer index for the type of code in SerialInterface::MeasurementCodes or SerialInterface::ControlCodes
 *         If return value >= CONTROL_CODES__RETURN_OFFSET, its a ControlCode, else its MeasurementCode. This is done to be able to differentiate the two
 *         with a single output (I dont think there will be 100 MeasurementCodes, so this should work with no problem)
 *         
 */
int SerialInterface::readCode(){
    //size_t buffer_idx = 0; unsused
    int pos = -1;
    int c1, c2;

    if(Serial1.available() >= 2){
        c1 = Serial1.read();
        c2 = Serial1.read();

        pos = codeExists(c1,c2);
    }
    
    return pos;
}

