#include <Arduino.h>
#include <stdio.h>
#include <time.h>

#include "Sensor.h"
#include "SerialInterface.h"

#define HUMIDITY_ARRAY_POS 5
#define TEMPERATURE_ARRAY_POS 4
#define RAIN_ARRAY_POS 3
#define CLEAN_ARRAY_POS 1
#define DIRTY_ARRAY_POS 2
#define SOILING_PERCENTAGE_ARRAY_POS 0

#define PUMP_PIN 16
// Changed to uint from define because it can be changed
uint TIMER_INTERVAL_MS_MEASUREMENTS = 10000;//3600000 // 1 hour in ms
uint TIMER_INTERVAL_MS_CLEANUP = 360000000;//3540000; // 59 min in ms
uint CLEANING_SYSTEM_ON_TIME = 3000;

// Timer FLags && Counters

volatile bool takeMeasurements = false;
volatile bool cleanUpReady = false;
unsigned long globalTimer = 0;

volatile bool manualCleanCommandReceived = false;
volatile bool collectDataCommandReceived = false;
volatile bool dataSucessfullyStored = false;
volatile bool dataUnsucessfullyStored = false;
volatile bool packetsUnsucessfullyBuilt = false;
volatile bool packetsSucessfullyBuilt = false;
volatile bool packetsSucessfullySent = false;
volatile bool packetsUnsucessfullySent = false;
volatile bool panelsCleaned = false;
volatile bool valuesSuccessfullyRead = false;
volatile bool valuesUnsuccessfullyRead = false;
volatile bool cleanSystem = false;
volatile bool readUserData = false;
volatile bool pumpIsOn = false;

SerialInterface serialInterface;

// Starting state is 0, MainController is the variable of each state
int MainController = 0;
bool readInputs, storeData, buildPackets, sendPackets;
//Sensor humidity = Sensor("Humidity");
//Sensor temperature = Sensor("Temperature");
Sensor HT = Sensor("HumidityTemperature");
Sensor rain = Sensor("Rain");
Sensor cleanPanelCurrent = Sensor("CleanPanelCurrent");
Sensor dirtyPanelCurrent = Sensor("DirtyPanelCurrent");

float sensor_deviation = 1.0495;
// Sensor values read

float valorLidoHumidade = 0;
float valorLidoTemperature = 0;
float valorLidoRain = 0;
float valorLidoCurrentClean = 0;
float valorLidoCurrentDirty = 0;

float valuesArray[6] = {0};

int userValue = 0, code = 0;
///// Timer FLags /////
void timerCallbackMeasure()
{
  takeMeasurements = true;

}

void timerCallbackClean()
{
  cleanUpReady = true;
}

//////////////////////
int k ;
unsigned long prevTime = 0 ,prevCleanTime = 0;
bool ON;


void setup() 
{
  Serial.begin(9600);
  Serial1.begin(9600);

  Wire.begin();
  cleanPanelCurrent.activate();
  dirtyPanelCurrent.activate();

  k = 0;
  globalTimer = 0;
  prevTime = 0;
  prevCleanTime = 0;
  ON = false;

  delay(2000);
}

void loop()
{
  if (prevCleanTime >= millis()){
    prevCleanTime = millis();
  }

  globalTimer = millis();
  if (millis() - prevTime >= 1000){
    if (!ON){      
      digitalWrite(LED_BUILTIN, HIGH);
      ON = true;
    } 
    else{  
      digitalWrite(LED_BUILTIN, LOW); 
      ON = false;
    }
    Serial.print("[");
    Serial.print(k);
    Serial.print(" s] MainController = ");
    Serial.println(MainController);
    k++;
  }
  
  if (millis() - prevCleanTime >= CLEANING_SYSTEM_ON_TIME && pumpIsOn){
    Serial.println("Turning pump off...");
    digitalWrite(PUMP_PIN, HIGH);
    pumpIsOn = false;
  }
	// MainController
  if ( (MainController == 0) && ( cleanUpReady )) 	                                                                                                                                          {MainController = 5;cleanUpReady = false;} else
	if ( (MainController == 0) && ( takeMeasurements ))  		                                                                                                                                    {MainController = 1;takeMeasurements = false;} else
	if ( (MainController == 0) && ( manualCleanCommandReceived )) 		                                                                                                                          {MainController = 5;manualCleanCommandReceived = false;} else
	if ( (MainController == 0) && ( collectDataCommandReceived )) 	                                                                                                                          	{MainController = 1;collectDataCommandReceived = false;} else
	if ( (MainController == 1) && ( valuesSuccessfullyRead ))			                                                                                                                              {MainController = 2;valuesSuccessfullyRead=false;} else
  if ( (MainController == 1) && ( valuesUnsuccessfullyRead )) 			                                                                                                                          {MainController = 1;valuesUnsuccessfullyRead = false;} else
	if ( (MainController == 2) && ( dataSucessfullyStored ))  		                                                                                                                           	  {MainController = 3;dataSucessfullyStored=false;} else
	if ( (MainController == 2) && ( dataUnsucessfullyStored )  )	   		                                                                                                                        {MainController = 2;dataUnsucessfullyStored=false;} else
	if ( (MainController == 3) && ( packetsUnsucessfullyBuilt ))	 		                                                                                                                          {MainController = 3;packetsUnsucessfullyBuilt=false;} else
  if ( (MainController == 3) && ( packetsSucessfullyBuilt ))		    		                                                                                                                      {MainController = 4;packetsSucessfullyBuilt=false;} else
  if ( (MainController == 4) && ( packetsSucessfullySent ))  	       		                                                                                                                      {MainController = 0;packetsSucessfullySent=false;} else
	if ( (MainController == 4) && ( packetsUnsucessfullySent ))	        		                                                                                                                    {MainController = 4;packetsUnsucessfullySent=false;} else
  if ( (MainController == 5) && ( panelsCleaned ))	             		                                                                                                                          {MainController = 0;panelsCleaned=false;}

    //These are the outputs, we may change this to call functions instead of setting flags to true or false.
	  readUserData = (MainController == 0);
    readInputs  = (MainController == 1);
    storeData   = (MainController == 2);
    buildPackets = (MainController == 3);
    sendPackets = (MainController == 4);
    cleanSystem = (MainController == 5);

    //Read inputs
    if(readUserData){
      code = serialInterface.readCode();
      if(code != -1){
          userValue = serialInterface.readValue();
          Serial.print("Received: ");
          Serial.print(serialInterface.getCode(code).c_str());
          Serial.print(":");
          Serial.println(userValue);

          // Then, depending on the code do something
          if(code == 0){
            TIMER_INTERVAL_MS_MEASUREMENTS = int(userValue)*1000;
          }
          else if(code == 1){
            TIMER_INTERVAL_MS_CLEANUP = int(userValue)*1000;
          }
          else if(code == 2){
            CLEANING_SYSTEM_ON_TIME = int(userValue)*1000;
          }
          else if(code == 3){
            // Clean now
            MainController = 5;
          }
          else if(code == 4){
            //Measure now
            MainController = 0;
            takeMeasurements = true;
          }
      }
    }
    else if(readInputs)
    {
      // temperature has to be read before humidity, else cant read humidity
      valorLidoTemperature = HT.readValue(1);
      valorLidoHumidade = HT.readValue(0);
      valorLidoRain = rain.readValue(0);
      valorLidoCurrentClean = cleanPanelCurrent.readValue(0);
      valorLidoCurrentDirty = dirtyPanelCurrent.readValue(0);

      valuesArray[HUMIDITY_ARRAY_POS] = valorLidoHumidade;
      valuesArray[TEMPERATURE_ARRAY_POS] = valorLidoTemperature;
      valuesArray[RAIN_ARRAY_POS] = valorLidoRain;
      valuesArray[CLEAN_ARRAY_POS] = valorLidoCurrentClean;
      valuesArray[DIRTY_ARRAY_POS] = valorLidoCurrentDirty;

      if (valorLidoCurrentClean<0)
        valorLidoCurrentClean *= -1;

      if (valorLidoCurrentDirty<0)
        valorLidoCurrentDirty *= -1;
      //valuesArray[SOILING_PERCENTAGE_ARRAY_POS] = (valorLidoCurrentDirty > valorLidoCurrentClean) ? 1-(valorLidoCurrentDirty-valorLidoCurrentClean*sensor_deviation)*100 : 1-(valorLidoCurrentClean-valorLidoCurrentDirty*sensor_deviation)*100;
      valuesArray[SOILING_PERCENTAGE_ARRAY_POS] = (valorLidoCurrentDirty < valorLidoCurrentClean) ? 1-valorLidoCurrentDirty/valorLidoCurrentClean : 1-valorLidoCurrentClean/valorLidoCurrentDirty;
      //  After reading inputs, go to store data
      valuesSuccessfullyRead = true;
    }
    else if (storeData){
      dataSucessfullyStored = true;
    }
    else if(buildPackets){
      packetsSucessfullyBuilt=true;
    }
    else if(sendPackets)
    {
      Serial.println("Sending data...");
      serialInterface.writeData(valuesArray, 6);
      packetsSucessfullySent = true;
    }
    else if(cleanSystem){
      /*
        Insert function and timer
      */
      prevCleanTime = millis();
      digitalWrite(PUMP_PIN, LOW);
      Serial.println("Turning pump on...");
      pumpIsOn = true;
      panelsCleaned = true;
    }

    // update timers

    // This means its raining, start waiting when its raining
    if(valuesArray[RAIN_ARRAY_POS] < 500){
      prevCleanTime = millis();
    }

    if( (globalTimer - prevCleanTime) >= TIMER_INTERVAL_MS_CLEANUP)//% TIMER_INTERVAL_MS_CLEANUP) < 1000 && mayClean)
    {
      timerCallbackClean();
    }


    if( (globalTimer % TIMER_INTERVAL_MS_MEASUREMENTS)  < 1000)
    {
      timerCallbackMeasure();

    }
  

    prevTime = globalTimer;
    delay(1000);
}
