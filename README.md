# SenSuji

Welcome to the repository of SenSuji, the monitoring station for your PV Panel farm that determines how much power you are losing to soiling.  


In this repository you will find:

- [ ] Source code for microcontroller  
- [ ] Sensors used and their documentation  
- [ ] Communication protocol between station and PV plant   
- [ ] API for receiving the data in the PV plant 
- [ ] Documentation about the automatic cleaning system  


In the [Wiki](https://gitlab.com/ivzvvv/sensuji/-/wikis/home) you can find additional information about the project and its development.


## Control System:

- [ ] uC Contains the PlatformIO project flashed in the Raspberry Pi Pico
- [ ] PV Central simulation with GUI and terminal interface. To be used with MySQL database, database configuration should be done in the conf.ini file.

The GUI has only been tested on a Linux environment (Ubuntu). For windows, use the Serial2DB_API and in this case conf.ini has to be on the same folder as Serial2DB_API.py.

